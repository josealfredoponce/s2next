<!DOCTYPE html>
<html lang="es">
<head>
	<title>A new Menu item</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


</head>
<body>

    <!-- Manager Menu -->
    <?php include_once('nav.php'); ?>


    <!-- Form add menu item -->
	<div class="container mt-4"> 
		<div class="row">
			<div class="col-sm-4">

				<form method="post" action="?action=add">

				  <div class="form-group">
				    <label for="exampleFormControlSelect1">Select Pattern</label>
				    <select name="pattern_id" class="form-control" id="exampleFormControlSelect1">
				      <option value="0">Ninguno</option>
						<?php foreach($select as $sl){ ?>

							<option value="<?php print $sl['id']; ?>"> <?php print $sl['name']; ?> </option>

						<?php } ?>

				    </select>
				    <small id="emailHelp" class="form-text text-muted">Si no selecciona un padre sera guardado como menu padre.</small>
				  </div>


				  <div class="form-group">
				    <label for="exampleFormControlInput1">Menu Name</label>
				    <input name="name"  required type="text" class="form-control" id="exampleFormControlInput1" placeholder="nombre menu">
				  </div>

				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Description</label>
				    <textarea name="description" required class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
				  </div>
				  <button type="submit" class="btn btn-primary mb-2">Save</button>
				</form>

			</div>
		</div>
	</div>


</body>
</html>