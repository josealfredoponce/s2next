<!DOCTYPE html>
<html lang="es">
<head>
	<title>Menu-List</title>

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>

    <!-- Manager Menu -->
    <?php include_once('nav.php'); ?>

    <!-- List menus -->
	<div class="container mt-3"> 
		<div class="row">
			<div class="col-sm-12">

				<table class="table">
				  <thead class="thead-dark">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Name</th>
				      <th scope="col">Description</th>
				      <th scope="col">Pattern</th>
				      <th scope="col">Edit</th>
				      <th scope="col">Delete</th>
				    </tr>
				  </thead>
				  <tbody>

				  	<?php foreach($listMenu as $list){?>
				    <tr>
				      <th scope="row"><?php print $list['id']; ?></th>
				      <td><?php print $list['name']; ?></td>
				      <td><?php print $list['description']; ?></td>
				      <td><?php print (empty($list['pattern_id'])) ? '' : $listMenu[$list['pattern_id']]['name']; ?></td>
				      <td><a class="btn btn-outline-info" href="?action=update&id=<?php print $list['id']; ?>" role="button">Update</a></td>
				      <td><a onclick="return confirm('¿Esta seguro de eliminar?');" class="btn btn-outline-danger delete" href="?action=delete&id=<?php print $list['id']; ?>" role="button">Delete</a></td>
				    </tr>
					<?php } ?>

				  </tbody>
				</table>

			</div>
		</div>
	</div>



</body>
</html>