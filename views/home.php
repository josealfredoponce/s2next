<!DOCTYPE html>
<html lang="es">
<head>
	<title>Menu - Manager</title>

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>


    <!-- Manager Menu -->
    <?php include_once('nav.php'); ?>

    <!-- Dinamic Submenu -->
    <div class="container mt-3"> 
    	<div class="row">
    		<div class="col-sm-12">

    			<?php foreach($arrMenu as $htmlMenu){ ?>
                    <div class="btn-group">
                    <button data-description="<?php print $htmlMenu['description']; ?>" type="button" class="btn btn-danger btn-message"><?php print strtoupper($htmlMenu['name']); ?></button>
                    <?php if( count($htmlMenu['childs']) > 0 ){ ?>

                            <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                                <?php foreach($htmlMenu['childs'] as $ch){ ?>                            
                                    <a data-description="<?php print $ch['description']; ?>" class="dropdown-item" href="#"><?php print strtoupper($ch['name']); ?></a>
                                <?php } ?>
                            </div>

                    <?php } ?>
                    
                    </div>
    			<?php } ?>

    		</div>
    	</div>

        <div class="row">
            <div class="col-sm-12">
                <div style="
                border:2px dashed #333;
                padding:20px;
                margin:0 auto;
                width: 30%;
                top:calc(25vh);
                position: relative;
                font-size:20px;
                text-align: center;
                " id="description"></div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        jQuery(document).ready(function($){
            $(".dropdown-menu a,.btn-message").click(function (event) {
                event.preventDefault();
                var desc = $(this).data('description');
                //console.log(desc);
                $("#description").text(desc);
            });
        });
    </script>

</body>
</html>