<?php

    class Config{

        private $servername;
        private $username;
        private $password;
        private $dbname;
        protected $conn;

        function __construct($arr){
            $this->servername = $arr['servername'];
            $this->username = $arr['username'];
            $this->password = $arr['password'];
            $this->dbname = $arr['dbname'];

            try {
                $this->conn = new PDO("mysql:host={$this->servername};dbname={$this->dbname}", $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //return $conn;
            }catch(PDOException $e) {
                $this->conn = null;
                #echo "Connection failed: " . $e->getMessage();
                //return false;
            }


        }


    }
