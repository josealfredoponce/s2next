<?php 


	class MenuModel extends Config{

		function __construct($conn){
			parent::__construct($conn);
		}

		function getAll(){
			$sql = 'select * from menu';
			$query = $this->conn->query($sql);
			$res = array();			
			while ($response = $query->fetch()) {
    			$res[] = $response;
			}
			return $res;
		}

		function getPatterns(){
			$sql = 'select * from menu where pattern_id = 0';
			$query = $this->conn->query($sql);
			$res = array();			
			while ($response = $query->fetch()) {
    			$res[] = $response;
			}
			return $res;
		}

		function insertMenu($request){
			extract($request);
			$sql = "INSERT INTO menu (name, description, pattern_id) VALUES (?,?,?)";
			try {
				$res = $this->conn->prepare($sql)->execute([$name,$description,$pattern_id]);
			}catch (Exception $e){
				return $e;
			}
			return $res;
		}

		function getMenu($id){
			$sql = "select * from menu where id = {$id}";
			$query = $this->conn->query($sql);
			$response = array();	
			$response = $query->fetch();
			return $response;
		}


		function updateMenu($request){
			extract($request);
			$sql = "UPDATE menu SET name=?, description=?, pattern_id=? WHERE id=?";
			$result = $this->conn->prepare($sql)->execute([$name, $description, $pattern_id, $id]);
			return $result;
		}

		function deleteMenu($id){
			$result = $this->conn->prepare("DELETE FROM menu WHERE id=?")->execute([$id]);
			return $result;
		}


	}