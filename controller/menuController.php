<?php 

	include_once('Model/Config.php');
	include_once('Model/MenuModel.php');


	class menuController
	{

		//if method not exist
		public function __call($method, $args)
		{
			include_once('views/error.php');
		}

		//menu principal
		public function home($model){

			$objMenu = new MenuModel($model);
			//por si no regresa nada
			$menus = array();
			$menus = $objMenu->getAll();
			//por si no pasa el foreach
			$arrMenu = array();

			//Array Operators=)  para no hacer nuevas consultas trabajamos con el
			// mismo array de una sola consulta , el patron se repite en otros metodos abajo
			foreach ($menus as $m) {				
				if($m['pattern_id'] == 0){
					$pattern = $m['id'];
					$arrMenu[$pattern] = $m;
					$childs = array();
					foreach ($menus as $child) {
						if($child['pattern_id'] == $pattern){
							//trae sus hijos 
							$childs[] = $child;
						}
					}
					//agrega sus hijos a su mismo arreglo
					$arrMenu[$pattern]['childs'] = $childs;

				}				
			}

			include_once('views/home.php');
		}

		//listado de menu con eventos update,delete
		public function list($model){
			$objMenu = new MenuModel($model);
			$menus = array();
			$menus = $objMenu->getAll();
			$listMenu = array();
			foreach ($menus as $m) {
				$listMenu[$m['id']] = $m;
			}
			include_once('views/list.php');
		}

		//creste new menu
		public function add($model){
			
			$method = $_SERVER['REQUEST_METHOD'];
			$objMenu = new MenuModel($model);

			if ($method == 'GET'){

				$patterns = array();
				$patterns = $objMenu->getPatterns();
				$select = array();
				foreach ($patterns as $p) {
					$select[] = $p;
				}			
				include_once('views/add.php');

			}elseif($method == 'POST'){

				$request['name'] = (empty($_POST['name'])) ? 'SIN NOMBRE' : $_POST['name'];
				$request['description'] = (empty($_POST['description'])) ? 'SIN DESCRIPCION' : $_POST['description'];
				$request['pattern_id'] = (empty($_POST['pattern_id'])) ? 0 : $_POST['pattern_id'];

				$response = $objMenu->insertMenu($request);
				//success generico
				include_once('views/success.php');
			}
		}


		//here update menu
		public function update($model){
			$method = $_SERVER['REQUEST_METHOD'];
			$objMenu = new MenuModel($model);

			if ($method == 'GET'){

				if(!empty($_GET['id'])){
					$menuId = $_GET['id'];
					$arrMenu = $objMenu->getMenu($menuId);
					//si no existe el id a editar
					if(empty($arrMenu)){ include_once('views/error.php'); return; }

					$patterns = array();
					$patterns = $objMenu->getPatterns();
					$select = array();
					foreach ($patterns as $p) {
						$select[] = $p;
					}

					include_once('views/updateMenu.php');

				}else{

					include_once('views/error.php');
				}

			}elseif($method == 'POST'){

				$request['id'] = (empty($_POST['id'])) ? 0 : $_POST['id'];
				$request['name'] = (empty($_POST['name'])) ? 'SIN NOMBRE' : $_POST['name'];
				$request['description'] = (empty($_POST['description'])) ? 'SIN DESCRIPCION' : $_POST['description'];
				$request['pattern_id'] = (empty($_POST['pattern_id'])) ? 0 : $_POST['pattern_id'];

				$response = $objMenu->updateMenu($request);
				
				include_once('views/success.php');

			}

		}

		//delte menu 
		public function delete($model){

			if(!empty($_GET['id'])){
				$menuId = $_GET['id'];
				$objMenu = new MenuModel($model);
				$response = $objMenu->deleteMenu($menuId);
				include_once('views/success.php');
			}

		}


	}



?>